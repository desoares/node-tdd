'use strict';

module.exports.parse = function (args, defaults, shorts) {
	var options = {};

	if (typeof defaults === 'object' && !(defaults instanceof Array)) {
		options = defaults;
	}

	if (typeof shorts === 'object' && !(defaults instanceof Array)) {
		for (var j in args) {
			var arg = args[j];

			if (arg.indexOf('-') === 0 && arg.indexOf('--') === -1) {
				arg = arg.substr(1);

				if (arg.indexOf('=') !== -1) {
					arg = arg.split('=');

					var keys = arg.shift(),
						value = arg.join('=');

					arg = keys.split('');

					var key = arg.pop();

					if (shorts.hasOwnProperty(key)) {
						key = shorts[key];
					}

					arg.push(key + '=' + value);
				} else {
					arg = arg.split('');
				}

				arg.forEach(function (key) {
					if (shorts.hasOwnProperty(key)) {
						key = shorts[key];
					}

					args.push('--' + key);
				});
			}
		}
	}

	for (var i in args) {
		var arg = args[i];

		if (arg.substr(0, 2) === '--') {
			arg = arg.substr(2);

			if (arg.indexOf('=') !== -1) {
				arg = arg.split('=');
				var key  = arg.shift();

				options[key] = arg.join('=');

				if (/^[0-9]+$/.test(options[key])) {
					options[key] = parseInt(options[key]);
				}
			} else {
				options[arg] = true;
			}

		}
	}

	return options;
};