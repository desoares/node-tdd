'use strict';

var tags = require('./lib/tags'),
	search = require('./lib/search');

var defaults = {
		'path': '.',
		'query': '',
		'depth': 2
	},
	shorts = {
		'p': 'path',
		'q': 'query',
		'd': 'depth',
		'h': 'help'
	};
	
tags = tags.parse(process.argv, defaults, shorts);

if (tags.help) {
	return console.log('Usage: ./app.js -q=query [-d=depth] [-p=path]');
} 

search.scan(tags.path, tags.depth, function (err, files) {
	search.match(tags.query, files).forEach(function (file) {
		console.log(file);
	});
});

