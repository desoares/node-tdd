var expect = require('chai').expect,
	search = require('../lib/search'),
	fs = require('fs');

describe('Search', function () {
	before(function () {
		if (!fs.existsSync('.testFiles')) {
			fs.mkdirSync('.test_files');
			fs.mkdirSync('.test_files/dir');
			fs.mkdirSync('.test_files/dir2');
			fs.writeFileSync('.test_files/a', '');
			fs.writeFileSync('.test_files/b', '');
			fs.writeFileSync('.test_files/dir/c', '');
			fs.writeFileSync('.test_files/dir2/d', '');
		}
	});

	after(function () {
		fs.unlinkSync('.test_files/a');
		fs.unlinkSync('.test_files/b');
		fs.unlinkSync('.test_files/dir/c');
		fs.unlinkSync('.test_files/dir2/d');
		fs.rmdirSync('.test_files/dir');
		fs.rmdirSync('.test_files/dir2');
		fs.rmdirSync('.test_files');
	});

	it('Should stop at a specified depth', function (done) {
		search.scan('test files', 1, function (err, fileList) {
			expect(fileList).to.deep.equal(
				[
					'.test_files/a',
					'.test_files/b'
				]
			);
		});
		done();
	});

	it('Should retrive files from specified directories', function (done) {
		search.scan('.test_files', 0, function (err, fileList) {
			expect(fileList).to.deep.equal(
				[
					'.test_files/a',
					'.test_files/b',
					'.test_files/dir/c',
					'.test_files/dir2/d'
				]
			);
			done();
		});
	});

});

describe ('#mach()', function () {
	it('Should find and return matches based on a query', function () {

		var files = ['hello.txt', 'world.js', 'another.js'],
			results = search.match('.js', files);

		expect(results).to.deep.equal(['world.js', 'another.js']);

		results = search.match('hello', files);
		expect(results).to.deep.equal(['hello.txt']);
	});
});