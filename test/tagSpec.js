var expect = require('chai').expect,
	tags = require('../lib/tags');

describe('Tags', function () {
	describe('tags.parse()', function () {
		it('Argument 0 shoult have a value of 4 integer, argument 1 string "fucking world"', function () {
			var args = ['--depth=4', '--hellow=fucking world'],
				results = tags.parse(args);

			expect(results).to.have.a.property('depth', 4);
			expect(results).to.have.a.property('hellow', 'fucking world');
		});

		it('sould fallback to defaults', function () {
			var args = ['--depth=4', '--hellow=fucking world'],
				defaults = {
					'depth': 2,
					'foo': 'bar?!?!?!? I don\'t get this.....'
				},
				results = tags.parse(args, defaults),
				expected = {
					'depth': 4, 
					'foo': 'bar?!?!?!? I don\'t get this.....',
					'hellow': 'fucking world'
				};

			expect(results).to.deep.equal(expected);
		});

		it('should accept tags with no value as a bool', function () {
			var args = ['--searchContentts'],
				results = tags.parse(args);

			expect(results).to.have.a.property('searchContentts', true);
		});

		it('should accept arguments with short tags', function () {
			var args = ['-sd=4', '-h'],
				shorts = {
					'd': 'depth',
					'h': 'hellow',
					's': 'searchContentts'
				},
				results = tags.parse(args, {}, shorts),
				expected = {
					'depth': 4,
					'hellow': true,
					'searchContentts': true
				};

			expect(results).to.deep.equal(expected);
		});
	});
});